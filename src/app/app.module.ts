import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ApiServiceService } from './api-service.service';
import { HttpClientModule } from '@angular/common/http';
// import { MatTableDataSource } from '@angular/material';
import { MaterialModule } from './materials/material.module'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    // MatTableDataSource,
    BrowserAnimationsModule,
    MaterialModule
  ],
  providers: [ApiServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
