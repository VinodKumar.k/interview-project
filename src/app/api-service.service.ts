import { Injectable } from '@angular/core';
import{ HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {


  url = "http://jsonplaceholder.typicode.com/photos";


  constructor(private http : HttpClient) { }

  getData(){

    return this.http.get(this.url );
  }
}
