import { Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ApiServiceService } from '../app/api-service.service';

import { employees } from './models/names';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'sample-pro';

   employee:  employees[] = [];

  displayedColumn= ['albumId','id','title','url','thumbnailUrl','actionsColumn'];
  dataSource: MatTableDataSource<employees>

  // @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private apiservice : ApiServiceService) {

  }

  ngOnInit(){
    this.getData();

  }

  deleterow(i){

    if (i >= 0) {
    this.employee.splice(i, 1);
     this.dataSource = new MatTableDataSource(this.employee);
     this.dataSource.paginator = this.paginator;
     this.dataSource.sort = this.sort;
  }

   }


  getData(){
    this.apiservice.getData().subscribe((result:any)=>{

          this.employee = result;
          this.dataSource = new MatTableDataSource(this.employee);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
    })
  }
}
